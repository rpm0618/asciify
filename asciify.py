#!/usr/bin/python

import Image, ImageFilter, ImageChops
from PIL import ImageOps

import numpy
from scipy import stats

import argparse

def monotone(img):
	"""Convert and image to black and white (not grayscale)"""
	
	temp = img.convert('L') #Convert the image to grayscale
	data = temp.load() #Get the data from the image
	sizeX, sizeY = temp.size
	
	#Loop through every pixel, turning it white if it's brighter than gray, turning it black otherwise
	for x in range(sizeX):
		for y in range(sizeY):
			if data[x, y] > 127:
				data[x, y] = 255
			else:
				data[x, y] = 0
	return temp

def getSlope(cell, strideX, strideY):
	"""Find a linear regression through the white values of the cell
	
	Returns:
	None		There is nothing in the cell
	Full		Most of the cell is full
	Vertical	The line of best fit is vertical
	(s, i)		A tuple containing the slope and intercept of the line of best fit
	"""

	#Create arrays containg the coordinates of each white pixel in the cell
	x, y = [], []
	for ix in range(strideX):
		for iy in range(strideY):
			if cell[ix, iy] == 255:
				x.append(ix)
				y.append(iy)
	
	#If there aren't enough points (or too many), a line can't be made	
	if len(x) < 2 or len(x) > 0.9 * strideX * strideY:
		return None
	
	try:
		#Calculate the line of best fit
		slope, intercept, r, p, std_dev = stats.linregress(x, y)
	except FloatingPointError:
		#If a FloatingPointError is raised, it means that the line of best fit is vertical
		return "Vertical"
	else:
		return (slope, intercept)

def getFillerChar(cell, strideX, strideY):
	"""If there was no edge detected, figure out which filler character should be used based on the value of the pixels"""

	#Create arrays containg the coordinates of each white pixel in the cell
	white, gray = [], []
	for ix in range(strideX):
		for iy in range(strideY):
			if cell[ix, iy] == 255:
				white.append(ix)
			if cell[ix, iy] >= 200:
				gray.append(ix)
	
	if len(white) > 0.9 * strideX * strideY: #If the space is primarily white
		return "#"			
	elif len(gray) > 0.9 * strideX * strideY: #If the space if primarily gray
		return "'"
	else: #If the space is primarily empty
		return " "
	
def getChar(cell, strideX, strideY):
	"""Get an ASCII character corresponding to the contents of each cell"""
	
	slope = getSlope(cell, strideX, strideY)
	
	#Take care of the special cases first
	if slope == None:
		return getFillerChar(cell, strideX, strideY)
	if slope == "Vertical":
		return "|"
	
	#If the slope is small, its a flat line
	if abs(slope[0]) < 0.5:
		#Use different characters depending on the height on the line within the cell
		if slope[1] > strideY / 2:
			return "-"
		else:
			return "_"
	
	#If the slope is very large, it might as well be vertical
	if abs(slope[0]) > 10:
		return "|"
	
	#Otherwise, just use the standard slashes for everything
	#NOTE: The slashes may seem reversed this is because the images was inverted at a previous point. Trust me, it works
	if slope[0] < 0:
		return "/"
	else:
		return "\\"

def getCell(data, x, strideX, y, strideY):
	"""Take a subsection of the data array, starting at (x,y) and ending at (x+strideX, y+strideY)"""
	
	cell = numpy.zeros((strideX, strideY))
	for ix in range(strideX):
		for iy in range(strideY):
				cell[ix, iy] = data[x+ix, y+iy]
	return cell
	
def asciify(img, strideX, strideY):
	"""Convert the image into a set of ASCII characters, using cells of size (strideX, strideY)"""
	
	data = img.load() #Load the image data
	sizeX, sizeY = img.size
	pic = "" #The resultant ascii image
	
	#Loop through every cell, getting the character for each cell and appending it onto the string
	for y in range(0, sizeY-strideY, strideY):
		for x in range(0, sizeX-strideX, strideX):
			cell = getCell(data, x, strideX, y, strideY)
			pic += getChar(cell, strideX, strideY)
		pic += "\r\n" #We've reached the end of one line, go to the next
	
	return pic
	
def main():

	#Set numpy to give warnings about dividing by zero (needed for the finding the slope of a vertical line)
	numpy.seterr(divide='raise', invalid='raise')

	#Command line option parsing
	parser = argparse.ArgumentParser()
	parser.add_argument("image", help="The image to asciify")
	parser.add_argument("-o", "--output", type=str, default="result.txt", help="The the name of the file to output, defaults to result.txt")
	parser.add_argument("-x", "--strideX", type=int, default=3, help="The width of each cell, defaults to 3")
	parser.add_argument("-y", "--strideY", type=int, default=5, help="The height of each cell, defaults to 5")
	parser.add_argument("-p", "--post", action="store_true", help="Output the image after processing")
	parser.add_argument("-f", "--fill", action="store_true", help="Fill in the solid areas")
	parser.add_argument("-i", "--invert", action="store_true", help="Invert the image before processing")
	args = parser.parse_args()
	
	#convert the input image to monotone
	img = ImageOps.posterize(Image.open(args.image), 1).convert('L')
	
	#Invert it if told to
	if args.invert:
		img = ImageOps.invert(img)
	
	#Find the edges of the picture
	edge = monotone(img.filter(ImageFilter.FIND_EDGES))
	
	#Depending on if we want to fill the image or not, use either the original or edgemap
	toascii = edge
	if args.fill:
		toascii = ImageChops.lighter(ImageOps.invert(img), edge) #Put the edges into the image, to enhance the ascii outline
	
	#Invert the image if told to (usually done in conjunction with a fill)
	if args.invert:
		toascii = toascii.point(lambda i : i * 2) #Brighten the image if it was inverted. Improves quality for some reason
	
	#Save the postprocess out if told to
	if args.post:
		toascii.save("post.bmp")
	
	#Asciify the image
	pic = asciify(toascii, args.strideX, args.strideY)
	
	#Save the picture out
	file = open(args.output, 'w')
	file.write(pic)
	
if __name__ == "__main__": main()